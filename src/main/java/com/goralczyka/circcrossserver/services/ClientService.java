package com.goralczyka.circcrossserver.services;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.*;
import org.apache.xmlrpc.common.TypeFactoryImpl;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by adam on 18.05.17.
 */
@Service
public class ClientService {

    private XmlRpcClient client;

    public ClientService() throws MalformedURLException {
        client = configureClient("http://localhost:8080/xmlrpc");
    }

    public Object execute(String pMethodName, Object[] objects) {
        try {
            return client.execute(pMethodName, objects);
        } catch (XmlRpcException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static XmlRpcClient configureClient(String s) throws MalformedURLException {
        XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        config.setServerURL(new URL(s));
        config.setEncoding(XmlRpcClientConfigImpl.UTF8_ENCODING);
        config.setEnabledForExceptions(true ); // !! [axe-180254 or [set true]]
        config.setEnabledForExtensions(true); // !! [axe-180254 or [set true]]
        config.setUserAgent("CT-XmlRpc");

        final XmlRpcClient rpcClient = new XmlRpcClient();
        rpcClient.setConfig(config);
        rpcClient.setTypeFactory(new TypeFactoryImpl(rpcClient)); // !! [axe-180254 or [remove statement]]
        rpcClient.setTransportFactory(
                new XmlRpcTransportFactory() {
                    @Override
                    public XmlRpcTransport getTransport() {
                        return new XmlRpcSun15HttpTransport(rpcClient);
                    }
                }
        );

        return rpcClient;
    }
}
