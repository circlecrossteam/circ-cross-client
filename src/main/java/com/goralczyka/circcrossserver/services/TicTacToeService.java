package com.goralczyka.circcrossserver.services;

import com.goralczyka.circcrossserver.components.SessionComponent;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * Created by adam on 20.05.17.
 */
@Log4j
@Service
public class TicTacToeService {

    @Autowired
    ClientService clientService;

    @Autowired
    SessionComponent sessionComponent;

    public Boolean newGame(final String gameToken) {
        String userToken = (String) clientService.execute("GameService.createGame", new String[] {gameToken});

        if (userToken == null) {
            return false; // nie udało się utworzyć gry
        }

        sessionComponent.userToken = userToken;
        sessionComponent.gameToken = gameToken;
        sessionComponent.isHost = true;
        return true;
    }

    public Boolean joinGame(final String gameToken) {
        String userToken = (String) clientService.execute("GameService.joinGame", new String[] {gameToken});

        if (userToken == null) {
            return false; // nie udało się dołączyć do gry
        }

        sessionComponent.userToken = userToken;
        sessionComponent.gameToken = gameToken;
        sessionComponent.isHost = false;
        return true;
    }

    public Boolean isGameReady() {
        return (Boolean) clientService.execute("GameService.isSecondPlayer",
                new String[] {sessionComponent.gameToken});
    }

    public Boolean isMyTurn() {
        Boolean isMyTurn = (Boolean) clientService.execute("GameService.isPlayerTurn",
                new String[] {sessionComponent.gameToken, sessionComponent.userToken});
        log.info("Gracz: " +
                sessionComponent.userToken + " - sprawdzam czy jest moj ruch w grze " + sessionComponent.gameToken + "\n" +
                "I okazuje się ze ruch jest mój? " + isMyTurn);
        return isMyTurn;
    }

    /**
     * //TODO: nazwa metody mega slaba, pozostalosc po tym co bylo... zmienic koniecznie
     * @return 0 - gra trwa, 1 - gra skonczona, przegrana; 2 - gra skonczona - remis
     */
    public Integer isGameEnded() {
        return (Integer) clientService.execute("GameService.isGameEnded",
                new String[] {sessionComponent.gameToken});
    }

    public Integer turn(Integer x, Integer y) {
        return (Integer) clientService.execute("GameService.turn",
                new Object[] {sessionComponent.gameToken, sessionComponent.userToken, x, y});
    }

    public HashMap<String, String> getBoard() {
        return (HashMap<String, String>) clientService.execute("GameService.getBoard",
                new String[] {sessionComponent.gameToken});
    }
}
