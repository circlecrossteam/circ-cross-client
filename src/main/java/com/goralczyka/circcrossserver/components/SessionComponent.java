package com.goralczyka.circcrossserver.components;

import org.springframework.stereotype.Component;

/**
 * Created by adam on 20.05.17.
 */
@Component
public class SessionComponent {

    // Token jest przyznawany tylko do jednej gry.
    // Jeżeli token istnieje, to znaczy że gracz jest w grze.
    // Jeżeli jest pusty lub jest nullem, gracz nie jest w żadnej grze.
    public String userToken = "";

    public String gameToken = "";

    public Boolean isHost = false;

    public void reset() {
        userToken = "";
        gameToken = "";
        isHost = false;
    }
}
