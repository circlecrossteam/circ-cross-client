package com.goralczyka.circcrossserver.forms;

import lombok.Data;

/**
 * Created by adam on 20.05.17.
 */
@Data
public class TurnForm {
    public Integer x;
    public Integer y;
}
