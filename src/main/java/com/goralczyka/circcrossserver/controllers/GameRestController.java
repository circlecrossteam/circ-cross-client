package com.goralczyka.circcrossserver.controllers;

import com.goralczyka.circcrossserver.components.SessionComponent;
import com.goralczyka.circcrossserver.services.TicTacToeService;
import com.goralczyka.circcrossserver.utils.SimpleResponse;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * Created by adam on 20.05.17.
 */
@Log4j
@RestController
public class GameRestController {

    @Autowired
    SessionComponent sessionComponent;

    @Autowired
    TicTacToeService ticTacToeService;

    @GetMapping("/isReady")
    public SimpleResponse isGameReady() {
        if (ticTacToeService.isGameReady()) {
            return new SimpleResponse.ResponseBuilder
                    ("Game " + sessionComponent.gameToken + " is ready", 0)
                    .build();
        }
        else {
            return new SimpleResponse.ResponseBuilder
                    ("Waiting for player in game " + sessionComponent.gameToken, 1)
                    .build();
        }
    }

    @GetMapping("/checkTurn")
    public SimpleResponse isMyTurn() {
        if (ticTacToeService.isMyTurn()) {
            return new SimpleResponse.ResponseBuilder
                    ("It is " + sessionComponent.userToken + " turn", 0)
                    .build();
        }
        else {
            return new SimpleResponse.ResponseBuilder
                    ("It is not" + sessionComponent.userToken + " turn", 1)
                    .build();
        }
    }

    @PostMapping("/turn")
    public SimpleResponse turn(@RequestParam Integer x, @RequestParam Integer y) {
        if (ticTacToeService.isMyTurn()) {
            Integer score = ticTacToeService.turn(x, y);
            if (score != -1) {
                log.info(sessionComponent.gameToken + " | " +
                        sessionComponent.userToken + " | " + score  );
                return new SimpleResponse.ResponseBuilder("Turn is ok", 0)
                        .withParameter("score", score.toString()).build();
            } else {
                return new SimpleResponse.ResponseBuilder("Problem with turn", 1).build();
            }
        }
        return new SimpleResponse.ResponseBuilder("It is not " + sessionComponent.userToken + " turn", -1)
                .build();
    }

    @GetMapping("/getBoard")
    public SimpleResponse getBoard() {
        HashMap<String, String> board = ticTacToeService.getBoard();

        return new SimpleResponse.ResponseBuilder("Board: ", 0).withParameters(board).build();
    }

    @GetMapping("/isEnded")
    public SimpleResponse isEnded() {
        Integer status = ticTacToeService.isGameEnded();
        if (status == 1) {
            return new SimpleResponse.ResponseBuilder("Przegrales", 0)
                    .withParameter("score", "1").build();
        } else if (status == 2) {
            return new SimpleResponse.ResponseBuilder("Remis", 0)
                    .withParameter("score", "2").build();
        } else {
            return new SimpleResponse.ResponseBuilder("Gra trwa", 0)
                    .withParameter("score", "0").build();
        }
    }
}
