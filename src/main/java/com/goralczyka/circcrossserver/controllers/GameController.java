package com.goralczyka.circcrossserver.controllers;

import com.goralczyka.circcrossserver.components.SessionComponent;
import com.goralczyka.circcrossserver.forms.GameTokenForm;
import com.goralczyka.circcrossserver.services.TicTacToeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class GameController {

	@Autowired
	TicTacToeService ticTacToeService;

	@Autowired
	SessionComponent sessionComponent;

	@GetMapping("/")
	public String welcome(Model model) {
		sessionComponent.reset();
		model.addAttribute("gameToken", new GameTokenForm());
		return "home";
	}

	@PostMapping("/newGame")
	public String createGame(@ModelAttribute("gameToken") GameTokenForm gameTokenForm, Model model) {
		if (ticTacToeService.newGame(gameTokenForm.getGameToken())) {
			model.addAttribute("gameToken", gameTokenForm.getGameToken());
			return "waiting"; // utworzono grę, odpalam ekran oczekiwania
		}
		else {
			//TODO: sypnac komunikatem bledu ze nie mozna utworzyc roomu
			return "redirect:/";
		}
	}

	@GetMapping("/board")
	public String showBoard(Model model) {
		if ("".equals(sessionComponent.gameToken) || sessionComponent.gameToken == null) {
			return "redirect:/";
		}
	    if (sessionComponent.isHost) {
	        model.addAttribute("playerMark", "X");
        } else {
            model.addAttribute("playerMark", "O");
        }
		if (!ticTacToeService.isGameReady()) {
			return "redirect:/";
		}
		else {
			return "board";
		}
	}

	@PostMapping("/joinGame")
	public String join(@ModelAttribute("gameToken") GameTokenForm gameTokenForm, Model model) {
        if (ticTacToeService.joinGame(gameTokenForm.getGameToken())) {
            return "redirect:/board";
        }
        else {
            return "redirect:/";
        }
    }
}
