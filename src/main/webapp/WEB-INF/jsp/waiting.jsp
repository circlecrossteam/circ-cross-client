<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
<head>
    <style>
        .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
        .outer {
            display: table;
            position: absolute;
            height: 100%;
            width: 100%;
        }

        .middle {
            display: table-cell;
            vertical-align: middle;
        }
        .inner {
            margin-left: auto;
            margin-right: auto;
            width: 200px;
        }
    </style>
    <title>Title</title>
</head>
<body>
    <div class="outer">
        <div class="middle">
            <div class="inner">
                <div class="row">
                    <div class="loader"></div>
                </div>
                <div class="row">
                    Your game token is: <c:out value="${gameToken}"></c:out>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    <script>
        checkGame();

        function checkGame() {
            setInterval(function(){
                getGameStatus();
            }, 5000);
        }
        
        function getGameStatus() {
            $.get('/isReady', function (data) {
                if (data.errorCode == 0) {
                    $(location).attr('href', '/board');
                }
            });
        }
    </script>

</body>
</html>
