<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>


    <style>
        .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
        .outer {
            display: table;
            position: absolute;
            height: 100%;
            width: 100%;
        }

        .middle {
            display: table-cell;
            vertical-align: middle;
        }
        .inner {
            margin-left: auto;
            margin-right: auto;
            width: 100px;
        }
    </style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

    <script>
        var myTurn = false;
        var playerSymbol="<c:out value="${playerMark}"/>";

        function updateBoard() {
            console.log("updateBoard");
            $.get("/getBoard", function (res) {
                console.log(JSON.stringify(res));
                document.getElementById("00").value = res.parameters.p00;
                document.getElementById("01").value = res.parameters.p01;
                document.getElementById("02").value = res.parameters.p02;

                document.getElementById("10").value = res.parameters.p10;
                document.getElementById("11").value = res.parameters.p11;
                document.getElementById("12").value = res.parameters.p12;

                document.getElementById("20").value = res.parameters.p20;
                document.getElementById("21").value = res.parameters.p21;
                document.getElementById("22").value = res.parameters.p22;
            });
        }

        function initGame() {
            checkTurn();
            updateBoard();
        }

        function checkScore() {
            $.get("/isEnded", function (res) {
                if (res.errorCode == 0) {
                    if (res.parameters.score === '1') {
                        alert("Przegrales");
                        $(location).attr('href', '/');
                    } else if (res.parameters.score === '2') {
                        alert("Remis");
                        $(location).attr('href', '/');
                    }
                }
            });
        }

        function checkTurn() {
            $.get("/checkTurn", function (res) {
                if (res.errorCode == 0) {
                    updateBoard();
                    checkScore();
                    myTurn = true;
                } else {
                    myTurn = false;
                }
            });
            if (myTurn) {
                console.log("its my turn");
            } else {
                console.log("its not my turn");
            }
        }

        function loopCheckTurn() {
            setInterval(function(){
                if (!myTurn) {
                    checkTurn();
                }
            }, 3000);
        }

        function callTurn(x, y) {
            $.ajax({
                type: "POST",
                url: "/turn",
                data: {
                    x: x,
                    y: y
                },
                success: function (res) {
                    // udalo sie zazanczyc
                    if (res.errorCode == 0) {
                        myTurn = false; // bo teraz ruch przeciwnika
                        var clickedFieldId = x.toString() + y.toString();
                        document.getElementById(clickedFieldId).value = playerSymbol;
                        if (res.parameters.score === '1') {
                            alert("WYGRALES");
                            $(location).attr('href', '/');
                        } else if (res.parameters.score === '2') {
                            alert("REMIS");
                            $(location).attr('href', '/');
                        }
                    }
                    else {
                        console.log(JSON.stringify(res));
                        checkTurn();
                    }
                }
            });
        }

        function clickField(x, y) {
            console.log("clickField " + x + " " + y);
            if (myTurn) {
                callTurn(x, y);
            }
        }
    </script>

    <title>Title</title>
</head>
<body>

        <div class="col-md-8" >
            <table border="1" >
                <tr>
                    <td><input type="button" id="00" style="height: 50px; width: 50px" onclick="clickField(0, 0)"/></td>
                    <td><input type="button" id="01" style="height: 50px; width: 50px" onclick="clickField(0, 1)"/></td>
                    <td><input type="button" id="02" style="height: 50px; width: 50px" onclick="clickField(0, 2)"/></td>
                </tr>
                <tr>
                    <td><input type="button" id="10" style="height: 50px; width: 50px" onclick="clickField(1, 0)"/></td>
                    <td><input type="button" id="11" style="height: 50px; width: 50px" onclick="clickField(1, 1)"/></td>
                    <td><input type="button" id="12" style="height: 50px; width: 50px" onclick="clickField(1, 2)"/></td>
                </tr>
                <tr>
                    <td><input type="button" id="20" style="height: 50px; width: 50px" onclick="clickField(2, 0)"/></td>
                    <td><input type="button" id="21" style="height: 50px; width: 50px" onclick="clickField(2, 1)"/></td>
                    <td><input type="button" id="22" style="height: 50px; width: 50px" onclick="clickField(2, 2)"/></td>
                </tr>
            </table>
        </div>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script>
        initGame();
        loopCheckTurn();
    </script>


</body>
</html>
