<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <style>
        .outer {
            display: table;
            position: absolute;
            height: 100%;
            width: 100%;
        }

        .middle {
            display: table-cell;
            vertical-align: middle;
        }
    </style>

    <title>Title</title>
</head>
<body>
    <div class="row">
        <div class="outer">
            <div class="middle">
                    <div class="col-md-3 col-md-offset-3 text-center">
                        <form:form action="/newGame" method="POST" modelAttribute="gameToken">
                            <form:input type="text" path="gameToken" id="newGameToken" placeholder="Game token"/>
                            <input type="submit" value="New Game">
                        </form:form>
                    </div>
                    <div class="col-md-3 text-center">
                        <form:form action="/joinGame" method="POST" modelAttribute="gameToken">
                            <form:input type="text" path="gameToken" id="joinGameToken" placeholder="Game token"/>
                            <input type="submit" value="Join Game">
                        </form:form>
                    </div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>